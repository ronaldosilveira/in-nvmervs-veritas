import calculavalordasletras as todas


def testa_se_calcula_todas_as_letras_do_nome_dah_retorno_correto():
    todas_as_letras = todas.numero_todas_as_letras('Alberto')
    assert 1 == todas_as_letras


def testa_se_calcula_corretamente_todas_as_vogais_de_um_nome():
    todas_as_vogais = todas.numero_todas_as_vogais('Carlos')
    assert 7 == todas_as_vogais


def testa_se_calcula_corretamente_todas_as_consoantes_de_um_nome():
    todas_as_vogais = todas.numero_todas_as_consoantes('Ana')
    assert 5 == todas_as_vogais
