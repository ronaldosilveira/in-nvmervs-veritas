def reducao_base(numero: int) -> int:
    """
        Sistema Base de redução Númerica para reduzir a complexidade da reduçao
            numerica quando necessário redução sucessivas

    Args:
        numero (int): o Número que será reduzido

    Returns:
        int: O Número resultado da redução
    """
    n = 0
    for numeros in str(numero):
        n = n + int(numeros)
    return n


def reducao_numerica_ate_1_algarismo(numero: int) -> int:
    """
        Reduz um número qualquer até ter um único algarismo conforme as regras
            da Redução Pitagórica

    Args:
        numero (int): Um Número inteiro que se deseja reduzir

    Returns:
        int: Um Número Interiro Positivo menor que 10

    >>> reducao_numerica_ate_1_algarismo(9)
    9
    >>> reducao_numerica_ate_1_algarismo(10)
    1
    """
    n = 0
    if numero < 10:
        return numero
    else:
        n = 0
        n = reducao_base(numero)
        if n < 10:
            return n
        else:
            return reducao_base(n)


def reducao_numerica_com_numeros_mestres(numero: int) -> int:
    """
        Reduz um número qualquer até ter um único algarismo ou um número Mestre
            conforme as regras da Redução Pitagórica

    Args:
        numero (int): Um Número inteiro que se deseja reduzir

    Returns:
        int: Um Número Interiro Positivo menor que 10

    >>> reducao_numerica_com_numeros_mestres(83)
    11
    >>> reducao_numerica_com_numeros_mestres(1984)
    22
    """
    if numero < 10:
        return numero
    else:
        n = 0
        n = reducao_base(numero)
        if n < 10 or n == 11 or n == 22:
            return n
        else:
            return reducao_base(n)
