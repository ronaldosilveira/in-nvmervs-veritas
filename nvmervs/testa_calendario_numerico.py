import calculavalordadata as d
import calendarionumerico as c
import pytest


def testa_se_o_calculo_do_ano_atual_estah_correto():
    ano = c.reducao_ano_atual('2025')
    assert 9 == ano


def testa_calculo_do_ano_pessoal():
    anopessoal = c.anopessoal('07091822', '2025')
    assert 7 == anopessoal


def testa_calculo_do_mes_pessoal():
    mespessoal = c.mespessoal('07091822', '07', '2025')
    assert 5 == mespessoal


def testa_calculo_de_dia_pessoal():
    diapessoal = c.diapessoal('07091822', '07', '07', '20025')
    assert 3 == diapessoal
