import pytest
import reducaonumerica as r


def testa_se_ocorre_a_reducao_numerica_com_numeros_1_algarismo():
    reduz = r.reducao_numerica_ate_1_algarismo(5)
    assert 5 == reduz


def testa_se_ocorre_a_reducao_numerica_com_numeros_2_algarismos_que_exigem_reducao_unica():
    reduz = r.reducao_numerica_ate_1_algarismo(45)
    assert 9 == reduz


def testa_se_ocorre_a_reducao_numerica_com_numeros_2_algarismos_que_exigem_redundancia():
    reduz = r.reducao_numerica_ate_1_algarismo(95)
    assert 5 == reduz


def testa_se_ocorre_a_reducao_numerica_com_numeros_3_algarismos_que_exigem_reducao_unica():
    reduz = r.reducao_numerica_ate_1_algarismo(141)
    assert 6 == reduz


def testa_se_ocorre_a_reducao_numerica_com_numeros_3_algarismos_que_exigem_redundancia():
    reduz = r.reducao_numerica_ate_1_algarismo(985)
    assert 4 == reduz


def testa_se_ocorre_a_reducao_numerica_com_numeros_4_algarismos_que_exigem_reducao_unica():
    reduz = r.reducao_numerica_ate_1_algarismo(1233)
    assert 9 == reduz


def testa_se_ocorre_a_reducao_numerica_com_numeros_4_algarismos_que_exigem_redundancia():
    reduz = r.reducao_numerica_ate_1_algarismo(1979)
    assert 8 == reduz


def testa_reducao_numerica_com_numeros_mestres_com_numeros_de_1_algarismo():
    reduz = r.reducao_numerica_com_numeros_mestres(9)
    assert 9 == reduz


def testa_reducao_numerica_com_numeros_mestres_com_numeros_de_2_algarismo():
    reduz = r.reducao_numerica_com_numeros_mestres(92)
    assert 11 == reduz


def testa_reducao_numerica_com_numeros_mestres_com_numeros_de_2_algarismos_resulta_nao_mestre():
    reduz = r.reducao_numerica_com_numeros_mestres(93)
    assert 3 == reduz
