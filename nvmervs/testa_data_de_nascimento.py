import calculavalordadata as d


def testa_se_le_dia_de_nascimento():
    dia_de_nascimento = d.dia_de_nascimento_sem_reducao('05071968')
    assert 5 == dia_de_nascimento


def testa_se_le_mes_de_nascimento():
    mes_de_nascimento = d.mes_de_nascimento_sem_reducao('28081998')
    assert 8 == mes_de_nascimento


def testa_se_le_ano_de_nascimento():
    ano_de_nascimento = d.ano_de_nascimento_sem_reducao('07091822')
    assert 1822 == ano_de_nascimento


def testa_dia_de_nascimento_reduzido():
    dia_de_nascimento_reduzido = d.dia_de_nascimento_reduzido('18122020')
    assert 9 == dia_de_nascimento_reduzido


def testa_mes_de_nascimento_reduzido():
    mes_de_nascimento_reduzido = d.mes_de_nascimento_reduzido('18122020')
    assert 3 == mes_de_nascimento_reduzido


def testa_ano_de_nascimento_reduzido():
    ano_de_nascimento_reduzido = d.ano_de_nascimento_reduzido('15022021')
    assert 5 == ano_de_nascimento_reduzido


def testa_se_ocorre_a_soma_de_todos_os_numeros_do_nascimento():
    nascimento_reduzido = d.data_de_Nascimento_reduzido('09071932')
    assert 4 == nascimento_reduzido


def testa_se_o_calculo_do_primeira_heranca_esta_correto():
    primeira_heranca = d.primeira_heranca('06052003')
    assert 1 == primeira_heranca


def testa_se_o_calculo_do_prmeira_heranca_ignora_negativo():
    primeira_heranca = d.primeira_heranca('02052003')
    assert 3 == primeira_heranca


def testa_se_o_calculo_do_segunda_heranca_esta_correto():
    segunda_heranca = d.segunda_heranca('06052003')
    assert 1 == segunda_heranca


def testa_se_o_calculo_do_segunda_heranca_ignora_negativo():
    segunda_heranca = d.segunda_heranca('02052003')
    assert 3 == segunda_heranca


def testa_se_o_primeiro_desafio_estah_correto():
    primeiro_desafio = d.primeiro_desafio('05081999')
    assert 2 == primeiro_desafio


def testa_se_o_segundo_desafio_estah_correto():
    segundo_desafio = d.segundo_desafio('05081999')
    assert 2 == segundo_desafio


def testa_se_o_carma_acumulado_estah_correto():
    carma = d.carma_acumulado('02121997')
    assert 17 == carma


def testa_primeiro_ciclo():
    primmeiro_ciclo = d.primeiro_ciclo('05102000')
    assert 6 == primmeiro_ciclo


def testa_segundo_ciclo():
    segunndo_ciclo = d.segundo_ciclo('05102000')
    assert 7 == segunndo_ciclo


def testa_terceiro_ciclo():
    tercceiro_ciclo = d.terceiro_ciclo('05102000')
    assert 4 == tercceiro_ciclo


def testa_quarto_ciclo():
    quarto_ciclo = d.quarto_ciclo('05102000')
    assert 3 == quarto_ciclo
