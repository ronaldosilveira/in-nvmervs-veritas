import reducaonumerica as r


def dia_de_nascimento_sem_reducao(nascimento: str) -> int:
    """
    Retorna o dia de nascimento a partir de uma data no formato de String.
        A Data desta função é retornada sem redução Numerica

    Args:
        nascimento (str): Uma data de nascimento no formato ddmmaaaa

    Returns:
        int: dia do nascimento sem redução

    >>> dia_de_nascimento_sem_reducao('28021952')
    28
    """
    dia = 0

    dia = int(nascimento[:2])

    return dia


def mes_de_nascimento_sem_reducao(nascimento: str) -> int:
    """
    Retorna o mes de nascimento a partir de uma data no formato de string.
        A data é retornada sem redução numérica

    Args:
        nascimento (str): Uma data de Nascimento qualquer no formato ddmmaaaa

    Returns:
        int: mes de nacimento sem redução numérica

    >>> mes_de_nascimento_sem_reducao('15111889')
    11
    """

    mes = 0

    mes = int(nascimento[2:4])

    return mes


def ano_de_nascimento_sem_reducao(nascimento: str) -> int:
    """
    Retorna o ano de nascimento a partir de uma data no formato de string.
        A data é retornada sem redução numérica

    Args:
        nascimento (str): Uma data de Nascimento qualquer no formato ddmmaaaa

    Returns:
        int: ano de nacimento sem redução numérica

    >>> ano_de_nascimento_sem_reducao('15111889')
    1889
    """
    ano = 0

    ano = int(nascimento[4:])

    return ano


def dia_de_nascimento_reduzido(nascimento: str) -> int:
    """
    Retorna o dia de nascimento a partir de uma data no formato de string.
        A data é retornada com redução numérica

    Args:
        nascimento (str): Uma data de Nascimento qualquer no formato ddmmaaaa

    Returns:
        int: dia de nacimento com redução numérica

    >>> dia_de_nascimento_reduzido('15111889')
    6
    """
    dia = 0

    dia = r.reducao_numerica_ate_1_algarismo(
        dia_de_nascimento_sem_reducao(nascimento)
    )

    return dia


def mes_de_nascimento_reduzido(nascimento: str) -> int:
    """
    Retorna o mes de nascimento a partir de uma data no formato de string.
        A data é retornada com redução numérica

    Args:
        nascimento (str): Uma data de Nascimento qualquer no formato ddmmaaaa

    Returns:
        int: mes de nacimento com redução numérica

    >>> mes_de_nascimento_reduzido('15111889')
    2
    """
    mes = 0
    mes = r.reducao_numerica_ate_1_algarismo(
        mes_de_nascimento_sem_reducao(nascimento)
    )
    return mes


def ano_de_nascimento_reduzido(nascimento: str) -> int:
    """
    Retorna o manoes de nascimento a partir de uma data no formato de string.
        A data é retornada com redução numérica

    Args:
        nascimento (str): Uma data de Nascimento qualquer no formato ddmmaaaa

    Returns:
        int: ano de nacimento com redução numérica

    >>> mes_de_nascimento_reduzido('15111889')
    2
    """
    ano = 0

    ano = r.reducao_numerica_ate_1_algarismo(
        ano_de_nascimento_sem_reducao(nascimento)
    )

    return ano


def data_de_Nascimento_reduzido(nascimento: str) -> int:
    """
    Retorna um número reduzido a um algarismo como redução de todos os
        numeros de uma data de nascimento no formato ddmmaaaa

    Args:
        nascimento (str): Uma data de Nascimento qualquer no formato ddmmaaaa

    Returns:
        int: numero reduzido de todos os algarismos da data de nascimento

    >>> data_de_Nascimento_reduzido('15021995')
    5
    """
    nasceu = 0

    for num in nascimento:
        nasceu += int(num)

    valor = r.reducao_numerica_ate_1_algarismo(nasceu)

    return valor


def primeira_heranca(nascimento: str) -> int:
    """
    Calcula a primeira herança a partir da data de nascimento

    Args:
        nascimento (str): data nascimento como string no formato ddmmaa

    Returns:
        int: valor da primeira herança

    >>> primeira_heranca('02041977')
    2
    """
    primeira_heranca = 0

    dia = dia_de_nascimento_reduzido(nascimento)
    mes = mes_de_nascimento_reduzido(nascimento)

    primeira_heranca = dia - mes

    return abs(primeira_heranca)


def segunda_heranca(nascimento: str) -> int:
    """
    Calcula a segunda herança a partir da data de nascimento

    Args:
        nascimento (str): data nascimento como string no formato ddmmaa

    Returns:
        int: valor da segunda herança

    >>> segunda_heranca('02041977')
    4
    """
    segunda_heranca = 0

    dia = dia_de_nascimento_reduzido(nascimento)
    ano = ano_de_nascimento_reduzido(nascimento)

    segunda_heranca = dia - ano
    return abs(segunda_heranca)


def primeiro_desafio(nascimento: str) -> int:
    """
    Calcula o primeiro desafio a partir da data de nascimento

    Args:
        nascimento (str): data nascimento como string no formato ddmmaa

    Returns:
        int: valor do primeiro desafio

    >>> primeiro_desafio('02041977')
    2
    """
    primeiro_desafio = 0

    primeiro_desafio = primeira_heranca(nascimento) - segunda_heranca(
        nascimento
    )

    return abs(primeiro_desafio)


def segundo_desafio(nascimento: str) -> int:
    """
    Calcula o segundo desafio a partir da data de nascimento

    Args:
        nascimento (str): data nascimento como string no formato ddmmaa

    Returns:
        int: valor do segundo desafio

    >>> segundo_desafio('02041977')
    2
    """
    segundo_desafio = 0

    ano = ano_de_nascimento_reduzido(nascimento)
    mes = mes_de_nascimento_reduzido(nascimento)

    segundo_desafio = ano - mes

    return segundo_desafio


def carma_acumulado(nascimento: str) -> int:
    carma = (
        primeiro_desafio(nascimento)
        + segundo_desafio(nascimento)
        + primeira_heranca(nascimento)
        + segunda_heranca(nascimento)
    )

    return carma


def primeiro_ciclo(nascimento: str) -> int:
    primeiro_ciclo = dia_de_nascimento_reduzido(
        nascimento
    ) + mes_de_nascimento_reduzido(nascimento)

    return primeiro_ciclo


def segundo_ciclo(nascimento: str) -> int:
    segundo_ciclo = dia_de_nascimento_reduzido(
        nascimento
    ) + ano_de_nascimento_reduzido(nascimento)

    return segundo_ciclo


def terceiro_ciclo(nascimento: str) -> int:
    terceiro_ciclo = segundo_ciclo(nascimento) + primeiro_ciclo(nascimento)

    valor = r.reducao_numerica_ate_1_algarismo(terceiro_ciclo)
    return valor


def quarto_ciclo(nascimento: str) -> int:
    quarto_ciclo = mes_de_nascimento_reduzido(
        nascimento
    ) + ano_de_nascimento_reduzido(nascimento)

    return quarto_ciclo
