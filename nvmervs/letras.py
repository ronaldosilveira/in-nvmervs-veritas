def EhVogal(letra: str) -> bool:
    """
        Recebe uma letra qualquer e retorna um booleano informando .Verdadeiro.
            para o caso de ser vogal e .Falso. para o caso de ser consoante.

        Args:
            letra (str): A letra que você deseja saber se é vogal ou consoante

    >>> EhVogal('A')
    True

    >>> EhVogal('B')
    False
    """

    if (
        letra == 'A'
        or letra == 'a'
        or letra == 'E'
        or letra == 'e'
        or letra == 'I'
        or letra == 'i'
        or letra == 'O'
        or letra == 'o'
        or letra == 'U'
        or letra == 'u'
        or letra == 'Y'
        or letra == 'y'
    ):
        return True
    else:
        return False
    pass


def TabelaPitagorica(letra: str) -> int:
    """
    Função que retorna um valor de 1 a 9 a partir do valor letra do alfabeto
        fornecido

    Args:
        letra (str): A Letra que será convertida como um Número conforme
            a tabela pitagórica

    >>> TabelaPitagorica('A')
    1
    >>> TabelaPitagorica('C')
    3
    >>> TabelaPitagorica('@')
    0
    >>> TabelaPitagorica('M')
    4
    >>> TabelaPitagorica('R')
    9
    """
    if (
        letra == 'A'
        or letra == 'a'
        or letra == 'J'
        or letra == 'j'
        or letra == 'S'
        or letra == 's'
        or letra == '1'
    ):
        return 1
    elif (
        letra == 'B'
        or letra == 'b'
        or letra == 'K'
        or letra == 'k'
        or letra == 'T'
        or letra == 't'
        or letra == '2'
    ):
        return 2
    elif (
        letra == 'C'
        or letra == 'c'
        or letra == 'L'
        or letra == 'l'
        or letra == 'U'
        or letra == 'u'
        or letra == '3'
    ):
        return 3
    elif (
        letra == 'D'
        or letra == 'd'
        or letra == 'M'
        or letra == 'm'
        or letra == 'V'
        or letra == 'v'
        or letra == '4'
    ):
        return 4
    elif (
        letra == 'E'
        or letra == 'e'
        or letra == 'N'
        or letra == 'n'
        or letra == 'W'
        or letra == 'w'
        or letra == '&'
        or letra == '5'
    ):
        return 5
    elif (
        letra == 'F'
        or letra == 'f'
        or letra == 'O'
        or letra == 'o'
        or letra == 'X'
        or letra == 'x'
        or letra == '6'
    ):
        return 6
    elif (
        letra == 'G'
        or letra == 'g'
        or letra == 'P'
        or letra == 'p'
        or letra == 'Y'
        or letra == 'y'
        or letra == '7'
    ):
        return 7
    elif (
        letra == 'H'
        or letra == 'h'
        or letra == 'Q'
        or letra == 'q'
        or letra == 'Z'
        or letra == 'z'
        or letra == '8'
    ):
        return 8
    elif (
        letra == 'I'
        or letra == 'i'
        or letra == 'R'
        or letra == 'r'
        or letra == '9'
    ):
        return 9
    else:
        return 0
