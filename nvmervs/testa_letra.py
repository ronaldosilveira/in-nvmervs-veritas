import letras as l
import pytest


def testa_se_a_funcao_rcorretamente_se_eh_vogal():
    vogal = l.EhVogal('F')
    assert False == vogal


def testa_se_a_tabela_pitagorica_r2_corretamente():
    pitagorico = l.TabelaPitagorica('k')
    assert 2 == pitagorico


def testa_se_a_tabela_pitagorica_r3_corretamente():
    pitagorico = l.TabelaPitagorica('L')
    assert 3 == pitagorico


def testa_se_a_tabela_pitagorica_r4_corretamente():
    pitagorico = l.TabelaPitagorica('v')
    assert 4 == pitagorico


def testa_se_a_tabela_pitagorica_r5_corretamente():
    pitagorico = l.TabelaPitagorica('W')
    assert 5 == pitagorico


def testa_se_a_tabela_pitagorica_r6_corretamente():
    pitagorico = l.TabelaPitagorica('X')
    assert 6 == pitagorico


def testa_se_a_tabela_pitagorica_r7_corretamente():
    pitagorico = l.TabelaPitagorica('y')
    assert 7 == pitagorico


def testa_se_a_tabela_pitagorica_r8_corretamente():
    pitagorico = l.TabelaPitagorica('q')
    assert 8 == pitagorico


def testa_se_a_tabela_pitagorica_r9_corretamente():
    pitagorico = l.TabelaPitagorica('I')
    assert 9 == pitagorico


def testa_se_a_tabela_pitagorica_r0_corretamente():
    pitagorico = l.TabelaPitagorica('%')
    assert 0 == pitagorico
