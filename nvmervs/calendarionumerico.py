import calculavalordadata as d
import calendarionumerico as c
import reducaonumerica as r


def reducao_ano_atual(anoatual: str) -> int:
    ano = r.reducao_numerica_ate_1_algarismo(int(anoatual))
    return ano


def anopessoal(nascimento: str, anoatual: str) -> int:
    anopessoal = 0
    ano = c.reducao_ano_atual(anoatual)
    dia = d.dia_de_nascimento_reduzido(nascimento)
    mes = d.mes_de_nascimento_reduzido(nascimento)
    anopessoal = int(ano + dia + mes)
    anopessoal = r.reducao_numerica_ate_1_algarismo(anopessoal)
    return anopessoal


def mespessoal(nascimento: str, mesatual: str, anoatual: str) -> int:
    mespessoal = 0

    meuanopessoal = anopessoal(nascimento, anoatual)

    mespessoal = r.reducao_numerica_ate_1_algarismo(
        int(mesatual) + int(meuanopessoal)
    )

    return mespessoal


def diapessoal(
    nascimento: str, diaatual: int, mesatual: str, anoatual: str
) -> int:
    diapessoal = 0
    meuanopessoal = anopessoal(nascimento, anoatual)
    valor = int(diaatual) + int(mesatual) + meuanopessoal
    diapessoal = r.reducao_numerica_ate_1_algarismo(valor)
    return diapessoal
