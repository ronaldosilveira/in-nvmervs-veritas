import letras as l
import reducaonumerica as r


def numero_todas_as_letras(nome: str) -> int:
    """
        Calcula o número do Destino que corresponde a todas as letras do nome
            a ser trabalhado.
        Nomes alternativos:
            - destino
            - auto expressão

    Args:
        nome (str): nome completo da pessoa que será analizada

    Returns:
        int: retorna o Número do destino (soma de todas as letras)

    >>> numero_todas_as_letras('Omar Mota')
    6
    """
    valor = 0

    for letra in nome:
        valor += l.TabelaPitagorica(letra)

    todas_as_letras = r.reducao_numerica_com_numeros_mestres(valor)

    return todas_as_letras


# vogais - automotivação
def numero_todas_as_vogais(nome: str) -> int:
    """Recebe um nome e calcula a soma de todas as vogais de um nome.

    Args:
        nome (str): Nome da Pessoa que se quer analisar

    Returns:
        int: Inteiro menor que 9 ou um número mestre

    >>> numero_todas_as_vogais('Ana')
    2
    """
    valor = 0

    for letra in nome:
        if l.EhVogal(letra):
            valor += l.TabelaPitagorica(letra)

    todas_as_vogais = r.reducao_numerica_com_numeros_mestres(valor)

    return todas_as_vogais


# consoantes - autoimagem
def numero_todas_as_consoantes(nome: str) -> int:
    valor = 0

    for letra in nome:
        if not l.EhVogal(letra):
            valor += l.TabelaPitagorica(letra)

    return valor
