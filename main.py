# ----------------------------------
# Nvmervs.py
# Calculo Numerologico no bash
# Licença: AGPLv3+
# Data: fev2025
# Autor: Ronaldo Silveira
# ----------------------------------

nome = ''
nascimento = ''


def imprimecabecalho():
    print('In Nvmervs Veritas')
    print('versao 0.0 - Por Ronaldo Silveira')
    print('------------------------------------')
    print('Licença AGPLv3+ - Use Software Livre\n')


def inputadados():
    print('Digite seu nome completo de nascimento')
    nome = input('Nome: ')

    print('Digite sua data de nascimento no formato ddmmyyyy')
    nascimento = input('Nascimento: ')


if __name__ == '__main__':
    imprimecabecalho()
    inputadados()
