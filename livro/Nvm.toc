\changetocdepth {4}
\babel@toc {brazil}{}\relax 
\contentsline {chapter}{Introdução}{5}{chapter*.3}%
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont I}}Conhecendo a Numerologia}{7}{part.1}%
\contentsline {chapter}{\chapternumberline {1}O que é Numerologia?}{9}{chapter.1}%
\contentsline {section}{\numberline {1.1}O que a Numerologia supõe?}{9}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Os Romanos}{9}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Hebreus}{10}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}O Universo é Número}{10}{section.1.2}%
\contentsline {chapter}{\chapternumberline {2}História da Numerologia}{11}{chapter.2}%
\contentsline {section}{\numberline {2.1}A Criação da Numerologia Pitagórica}{11}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}A origem do nosso alfabeto}{11}{subsection.2.1.1}%
\contentsline {chapter}{\chapternumberline {3}Pressupostos da Numerologia }{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}nome}{13}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Outro NOme}{13}{subsection.3.1.1}%
\contentsline {chapter}{\chapternumberline {4}Livre Arbítrio e Destino}{14}{chapter.4}%
\contentsline {section}{\numberline {4.1}nome}{14}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Outro NOme}{14}{subsection.4.1.1}%
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont II}}Pitágoras e os Números}{15}{part.2}%
\contentsline {chapter}{\chapternumberline {5}Quem foi Pitágoras}{16}{chapter.5}%
\contentsline {chapter}{\chapternumberline {6}Arquétipos}{17}{chapter.6}%
\contentsline {chapter}{\chapternumberline {7}A Energia dos Números}{18}{chapter.7}%
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont III}}Os Números e a Numerologia}{19}{part.3}%
\contentsline {chapter}{\chapternumberline {8}Classificação dos Números}{20}{chapter.8}%
\contentsline {section}{\numberline {8.1}A classificação dos Números}{20}{section.8.1}%
\contentsline {section}{\numberline {8.2}Pares e Ímpares}{20}{section.8.2}%
\contentsline {subsection}{\numberline {8.2.1}Números Pares}{21}{subsection.8.2.1}%
\contentsline {section}{\numberline {8.3}Números Elementares e Mestres}{22}{section.8.3}%
\contentsline {subsection}{\numberline {8.3.1}Números Básicos (ou Elementares)}{22}{subsection.8.3.1}%
\contentsline {subsection}{\numberline {8.3.2}O Zero}{22}{subsection.8.3.2}%
\contentsline {subsection}{\numberline {8.3.3}Números Mestres}{22}{subsection.8.3.3}%
\contentsline {subsubsection}{\numberline {8.3.3.1}Importância dos Números Mestres}{22}{subsubsection.8.3.3.1}%
\contentsline {subsection}{\numberline {8.3.4}Número Redutível}{23}{subsection.8.3.4}%
\contentsline {subsection}{\numberline {8.3.5}Números Nefastos}{23}{subsection.8.3.5}%
\contentsline {chapter}{\chapternumberline {9}Tipos de Numerologia}{24}{chapter.9}%
\contentsline {section}{\numberline {9.1}Tipos de Numerologia}{24}{section.9.1}%
\contentsline {subsection}{\numberline {9.1.1}Numerologia Pitagórica}{25}{subsection.9.1.1}%
\contentsline {subsection}{\numberline {9.1.2}Numerologia Védica}{25}{subsection.9.1.2}%
\contentsline {section}{\numberline {9.2}Numerologia Cabalística}{25}{section.9.2}%
\contentsline {section}{\numberline {9.3}Numerologia Quântica}{26}{section.9.3}%
\contentsline {chapter}{\chapternumberline {10}Integração da Numerologia}{28}{chapter.10}%
\contentsline {section}{\numberline {10.1}Com outros tipos de Numerologia}{28}{section.10.1}%
\contentsline {section}{\numberline {10.2}Com o Tarô}{28}{section.10.2}%
\contentsline {section}{\numberline {10.3}Com a Astrologia}{28}{section.10.3}%
\contentsline {chapter}{\chapternumberline {11}O Sete e o Treze}{30}{chapter.11}%
\contentsline {section}{\numberline {11.1}nome}{30}{section.11.1}%
\contentsline {subsection}{\numberline {11.1.1}Outro NOme}{30}{subsection.11.1.1}%
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont IV}}Preparando}{31}{part.4}%
\contentsline {chapter}{\chapternumberline {12}Tabela Pitagórica}{32}{chapter.12}%
\contentsline {section}{\numberline {12.1}A Tabela Pitagórica}{32}{section.12.1}%
\contentsline {subsection}{\numberline {12.1.1}Um Aviso}{32}{subsection.12.1.1}%
\contentsline {section}{\numberline {12.2}A Tabela}{32}{section.12.2}%
\contentsline {section}{\numberline {12.3}Como se chegou a esta tabela?}{32}{section.12.3}%
\contentsline {subsection}{\numberline {12.3.1}Ordenação}{32}{subsection.12.3.1}%
\contentsline {section}{\numberline {12.4}Redução Numérica}{33}{section.12.4}%
\contentsline {chapter}{\chapternumberline {13}O Valor das Letras}{36}{chapter.13}%
\contentsline {chapter}{\chapternumberline {14}Outras Tabelas Numerológicas}{37}{chapter.14}%
\contentsline {chapter}{\chapternumberline {15}Redução Numérica}{38}{chapter.15}%
\contentsline {section}{\numberline {15.1}Redução Numérica}{38}{section.15.1}%
\contentsline {section}{\numberline {15.2}Como fazer a Redução Numérica?}{38}{section.15.2}%
\contentsline {subsection}{\numberline {15.2.1}Datas}{38}{subsection.15.2.1}%
\contentsline {subsection}{\numberline {15.2.2}Nomes}{39}{subsection.15.2.2}%
\contentsline {subsection}{\numberline {15.2.3}Números Mestres}{39}{subsection.15.2.3}%
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont V}}Cálculos Básicos}{40}{part.5}%
\contentsline {chapter}{\chapternumberline {16}Idealidade}{41}{chapter.16}%
\contentsline {chapter}{\chapternumberline {17}Impressão}{42}{chapter.17}%
\contentsline {chapter}{\chapternumberline {18}Expressão}{43}{chapter.18}%
\contentsline {chapter}{\chapternumberline {19}Ausências Númericas}{44}{chapter.19}%
\contentsline {chapter}{\chapternumberline {20}Excessos Númericos}{45}{chapter.20}%
\contentsline {chapter}{\chapternumberline {21}Complementos}{46}{chapter.21}%
\contentsline {section}{\numberline {21.1}Cores}{46}{section.21.1}%
\contentsline {section}{\numberline {21.2}Pedras}{46}{section.21.2}%
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont VI}}Ciclos e Períodos}{47}{part.6}%
\contentsline {chapter}{\chapternumberline {22}Pináculos}{48}{chapter.22}%
\contentsline {chapter}{\chapternumberline {23}Ano Universal}{49}{chapter.23}%
\contentsline {chapter}{\chapternumberline {24}Ano Pessoal}{50}{chapter.24}%
\contentsline {chapter}{\chapternumberline {25}Mês Pessoal}{51}{chapter.25}%
\contentsline {chapter}{\chapternumberline {26}Dia Pessoal}{52}{chapter.26}%
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\contentsline {part}{\partnumberline {{\fontencoding {OT1}\selectfont VII}}Apêndices e Anexos}{53}{part.7}%
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\contentsline {appendix}{\chapternumberline {A}FDL}{54}{appendix.anexochapback.1}%
\contentsline {chapter}{\'Indice}{62}{section*.16}%
