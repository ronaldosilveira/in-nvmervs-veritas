# ------------------------------------------------------- #
# Script: Roda testes com o Coverage                      #
# Autor: Ronaldo Silveira                                 #
# Licença: AGPLv3+                                        #
# data: Janeiro de 2025                                   #
# ------------------------------------------------------- #

#!/usr/bin/env bash

coverage run -m pytest && coverage html

